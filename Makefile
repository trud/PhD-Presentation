# Project specific settings
DOCNAME = main
PDFNAME = main
OUTPUT_FOLDER = build

# LATEX tools
LATEXMK = latexmk
LATEXMK_FLAGS = -pdflatex=lualatex -pdf -jobname="$(OUTPUT_FOLDER)/$(PDFNAME)"

INDENT = latexindent
INDENT_FLAGS = -w -l -c=$(OUTPUT_FOLDER)

CHKTEX = bash checktex.sh

LACHECK = lacheck

# Targets
all: doc
doc: pdf

indent:
	$(INDENT) $(INDENT_FLAGS) $(DOCNAME).tex

lacheck:
	$(LACHECK) $(DOCNAME).tex

chk:
	$(CHKTEX) $(DOCNAME).tex

pdf: indent lacheck chk
	$(LATEXMK) $(LATEXMK_FLAGS) $(DOCNAME).tex
	cp $(OUTPUT_FOLDER)/$(PDFNAME).pdf .
